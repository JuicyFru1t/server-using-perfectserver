//
//  PhotoDocumentGateway.swift
//  PerfectTemplate
//
//  Created by Alexander Khodko on 16/05/2017.
//
//

import Foundation
import PerfectLib
import PerfectHTTP
import PerfectHTTPServer

class PhotoDocumentGateway {
    
    //создание
    class func create(request: HTTPRequest, _ response: HTTPResponse) {
        var payload: [String: Any]? = [:]
        do {
            payload = try request.postBodyString?.jsonDecode() as? [String: Any]
        } catch {
            try! response.setBody(json: ["error": "Неверные данные"])
        }
        if let userId = payload?["userId"] as? Int,
            let description = payload?["description"] as? String,
            let importancy = payload?["importancy"] as? Int,
            let date = payload?["date"] as? String,
            let data = payload?["data"] as? String {
            let newPhotoDocument = PhotoDocument(userId: userId, description: description, date: date, id: nil, imageData: data, importancy: importancy)
            if newPhotoDocument.create() == .commandOK {
                try! response.setBody(json: ["succses": "Ok"])
            } else {
                try! response.setBody(json: ["error": "Ошибка БД"])
            }
        } else {
            try! response.setBody(json: ["error": "Неверные данные payload"])
        }
        response.setHeader(.contentType, value: "application/json")
        response.completed()
    }
    
    //удаление
    class func delete(request: HTTPRequest, _ response: HTTPResponse) {
        var payload: [String: Any]? = [:]
        do {
            payload = try request.postBodyString?.jsonDecode() as? [String: Any]
        } catch {
            try! response.setBody(json: ["error": "Неверные данные"])
        }
        if let id = payload?["id"] as? Int {
            if let photoDocument = PhotoDocumentMapper.get(id: id) {
                if PhotoDocumentMapper.delete(photoDocument: photoDocument) == .commandOK {
                    try! response.setBody(json: ["succses": "Ok"])
                } else {
                    try! response.setBody(json: ["error": "Ошибка БД"])
                }
            } else {
                try! response.setBody(json: ["error": "Неверные данные payload шв"])
            }
        } else {
            try! response.setBody(json: ["error": "Неверные данные payload"])
        }
        response.setHeader(.contentType, value: "application/json")
        response.completed()
    }
}
