//
//  main.swift
//  PerfectTemplate
//
//  Created by Alexander Khodko on 15/05/2017.
//
//
import PerfectLib
import PerfectHTTP
import PerfectHTTPServer

var routes = Routes()
routes.add(method: .post, uri: "user/auth", handler: UserGateway.auth)
routes.add(method: .post, uri: "user/create", handler: UserGateway.create)
routes.add(method: .post, uri: "photo/create", handler: PhotoDocumentGateway.create)
routes.add(method: .post, uri: "photo/delete", handler: PhotoDocumentGateway.delete)
routes.add(method: .post, uri: "user/getPhotos", handler: UserGateway.getUserPhotos)
routes.add(method: .post, uri: "user/getPhotosWithParameters", handler: UserGateway.getPhotosWithParameters)

// Create server object.
let server = HTTPServer()

// Listen on port 8181.
server.serverPort = 8080

// Add our routes.
server.addRoutes(routes)

do {
    try server.start()
} catch PerfectError.networkError(let err, let msg) {
    print("Network error thrown: \(err) \(msg)")
}

