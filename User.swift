//
//  Univer.swift
//  PerfectTemplate
//
//  Created by Alexander Khodko on 15/05/2017.
//
//

import PostgreSQL
import Foundation

class User {
    
    var id: Int
    let name: String
    let password: String
    
    init(name: String, password: String, id: Int?) {
        self.name = name
        self.id = id ?? 0
        self.password = password
    }
    
    func auth() {
        UserMapper.search(user: self)
    }
    
    func create() -> PGResult.StatusType {
        return UserMapper.create(user: self)
    }
    
    func getPhotos() -> [PhotoDocument] {
        return PhotoDocumentMapper.search(id: self.id, query: "", method: "")
    }
    
    func getPhotosWithParameters(query: String, method: String) -> [PhotoDocument] {
        return PhotoDocumentMapper.search(id: self.id, query: query, method: method)
    }
}


