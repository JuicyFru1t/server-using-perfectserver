//
//  UserMapper.swift
//  PerfectTemplate
//
//  Created by Alexander Khodko on 16/05/2017.
//
//

import Foundation
import PostgreSQL

class UserMapper {
    
    //создание
    class func create(user: User) -> PGResult.StatusType {
        let name = user.name
        let password = user.password
        let p = PGConnection()
        let _ = p.connectdb("host=localhost port=5432 dbname=perfect")
        let result = p.exec(statement: "insert into  \"Users\" (\"name\", \"password\") values ($1, $2)", params: [name, password])
        p.close()
        return result.status()
    }
    
    //получение по логину и пароль
    class func search(user: User) {
        let name = user.name
        let password = user.password
        let p = PGConnection()
        let _ = p.connectdb("host=localhost port=5432 dbname=perfect")
        let res = p.exec(statement: "select id from  \"Users\" where name = ($1) and password = ($2)", params: [name, password])
        p.close()
        if let id = res.getFieldInt(tupleIndex: 0, fieldIndex: 0) {
            user.id = id
        }
    }
    
    //получение по id
    class func getById(id: Int) -> User? {
        let p = PGConnection()
        let _ = p.connectdb("host=localhost port=5432 dbname=perfect")
        let res = p.exec(statement: "select * from  \"Users\" where id = ($1)", params: [id])
        p.close()
        if res.numTuples() > 0 {
            let name = res.getFieldString(tupleIndex: 0, fieldIndex: 1)
            let password = res.getFieldString(tupleIndex: 0, fieldIndex: 2)
            let user = User(name: name!, password: password!, id: id)
            return user
        } else {
            return nil
        }
    }
}
