//
//  PhotoMode.swift
//  PerfectTemplate
//
//  Created by Alexander Khodko on 16/05/2017.
//
//

import Foundation
import PostgreSQL

class PhotoDocument {
    
    let id: Int
    let userId: Int
    let date: String
    let imageData: String
    let importancy: Int
    let description: String
    
    init(userId: Int, description: String, date: String, id: Int?, imageData: String, importancy: Int) {
        self.date = date
        self.id = id ?? 0
        self.imageData = imageData
        self.importancy = importancy
        self.description = description
        self.userId = userId
    }
    
    func create() -> PGResult.StatusType {
        return PhotoDocumentMapper.create(photoDocument: self)
    }
    
    var JSON: [String: Any] {
        get {
            return ["description": self.description,
                    "importancy": self.importancy,
                    "date": self.date,
                    "data": self.imageData,
                    "id": self.id]
        }
    }
}
