//
//  PhotoDocumentMapper.swift
//  PerfectTemplate
//
//  Created by Alexander Khodko on 16/05/2017.
//
//

import Foundation
import PostgreSQL

class PhotoDocumentMapper {
    
    //создание
    
    class func create(photoDocument: PhotoDocument) -> PGResult.StatusType {
        let description = photoDocument.description
        let date = photoDocument.date
        let imageData = photoDocument.imageData
        let importancy = photoDocument.importancy
        let userId = photoDocument.userId
        
        let p = PGConnection()
        let _ = p.connectdb("host=localhost port=5432 dbname=perfect")
        let result = p.exec(statement: "insert into  \"Photos\" (\"userId\", \"importancy\", \"photo\", \"description\", \"date\") values ($1, $2, $3, $4, $5)", params: [userId, importancy, imageData, description, date])
        p.close()
        
        return result.status()
    }
    
    //поиск фото по id пользователя
    class func search(id: Int, query: String, method: String) -> [PhotoDocument] {
        let p = PGConnection()
        let _ = p.connectdb("host=localhost port=5432 dbname=perfect")
        var result: PGResult!
        print(query, method)
        if query != "" {
            switch method {
            case "name": result = p.exec(statement: "select * from \"Photos\" where \"userId\" = $1 and \"description\" LIKE $2 order by \"description\"", params: [id, query])
            case "importancy": result = p.exec(statement: "select * from \"Photos\" where \"userId\" = $1 and \"description\" LIKE $2 order by \"importancy\" desc", params: [id, query])
            default: result = p.exec(statement: "select * from \"Photos\" where \"userId\" = $1 and \"description\" LIKE $2", params: [id, query])
            }
        } else {
            switch method {
            case "name": result = p.exec(statement: "select * from \"Photos\" where \"userId\" = $1 order by \"description\"", params: [id])
            case "importancy": result = p.exec(statement: "select * from \"Photos\" where \"userId\" = $1 order by \"importancy\" desc", params: [id])
            default: result = p.exec(statement: "select * from \"Photos\" where \"userId\" = $1", params: [id])
            }
        }
        p.close()
        
        var photos: [PhotoDocument] = []
        let resultsNumber = result.numTuples() - 1
        if resultsNumber != -1 {
            for index in 0...resultsNumber {
                let id = result.getFieldInt(tupleIndex: index, fieldIndex: 0)!
                let userId = result.getFieldInt(tupleIndex: index, fieldIndex: 1)!
                let importancy = result.getFieldInt(tupleIndex: index, fieldIndex: 2)!
                let imageData = result.getFieldString(tupleIndex: index, fieldIndex: 3)!
                let description = result.getFieldString(tupleIndex: index, fieldIndex: 4)!
                let date = result.getFieldString(tupleIndex: index, fieldIndex: 5)!
                
                let photo = PhotoDocument(userId: userId,
                                          description: description,
                                          date: date,
                                          id: id,
                                          imageData: imageData,
                                          importancy: importancy)
                photos.append(photo)
            }
        }
        return photos
    }
    
    class func get(id: Int) -> PhotoDocument? {
        
        let p = PGConnection()
        let _ = p.connectdb("host=localhost port=5432 dbname=perfect")
        let result = p.exec(statement: "select * from \"Photos\" where \"id\" = $1", params: [id])
        p.close()
        
        var photo: PhotoDocument?
        if result.numTuples() == 1 {
            let id = result.getFieldInt(tupleIndex: 0, fieldIndex: 0)!
            let userId = result.getFieldInt(tupleIndex: 0, fieldIndex: 1)!
            let importancy = result.getFieldInt(tupleIndex: 0, fieldIndex: 2)!
            let imageData = result.getFieldString(tupleIndex: 0, fieldIndex: 3)!
            let description = result.getFieldString(tupleIndex: 0, fieldIndex: 4)!
            let date = result.getFieldString(tupleIndex: 0, fieldIndex: 5)!
            
            photo = PhotoDocument(userId: userId,
                                  description: description,
                                  date: date,
                                  id: id,
                                  imageData: imageData,
                                  importancy: importancy)
        }
        return photo
    }
    
    class func delete(photoDocument: PhotoDocument) -> PGResult.StatusType {
        let id = photoDocument.id
        
        let p = PGConnection()
        let _ = p.connectdb("host=localhost port=5432 dbname=perfect")
        let result = p.exec(statement: "delete from  \"Photos\" where \"id\" = $1", params: [id])
        p.close()
        
        return result.status()
    }
}


