//
//  UserGateway.swift
//  PerfectTemplate
//
//  Created by Alexander Khodko on 16/05/2017.
//
//

import Foundation
import PerfectLib
import PerfectHTTP
import PerfectHTTPServer

class UserGateway {
    
    //авторизация
    class func auth(request: HTTPRequest, _ response: HTTPResponse) {
        var payload: [String: Any]? = [:]
        do {
            payload = try request.postBodyString?.jsonDecode() as? [String: Any]
        } catch {
            try! response.setBody(json: ["error": "Неверные данные"])
        }
        if let name = payload?["name"] as? String, let password = payload?["password"] as? String {
            let newUser = User(name: name, password: password, id: nil)
            newUser.auth()
            if newUser.id != 0 {
                try! response.setBody(json: ["userId": newUser.id])
            } else {
                try! response.setBody(json: ["error": "Пользователя не существует"])
            }
        } else {
            try! response.setBody(json: ["error": "Неверные данные payload"])
        }
        response.setHeader(.contentType, value: "application/json")
        response.completed()
    }
    
    //регистрация
    class func create(request: HTTPRequest, _ response: HTTPResponse) {
        var payload: [String: Any]? = [:]
        do {
            payload = try request.postBodyString?.jsonDecode() as? [String: Any]
        } catch {
            try! response.setBody(json: ["error": "Неверные данные"])
        }
        if let name = payload?["name"] as? String, let password = payload?["password"] as? String {
            let newUser = User(name: name, password: password, id: nil)
            if newUser.create() == .commandOK {
                newUser.auth()
                if newUser.id != 0 {
                    try! response.setBody(json: ["userId": newUser.id])
                } else {
                   try! response.setBody(json: ["error": "serverError"])
                }
            } else {
                try! response.setBody(json: ["error": "Пользователь существует"])
            }
        } else {
            try! response.setBody(json: ["error": "Неверные данные payload"])
        }
        response.setHeader(.contentType, value: "application/json")
        response.completed()
    }
    
    class func getUserPhotos(request: HTTPRequest, _ response: HTTPResponse) {
        var payload: [String: Any]? = [:]
        do {
            payload = try request.postBodyString?.jsonDecode() as? [String: Any]
        } catch {
            try! response.setBody(json: ["error": "Неверные данные"])
        }
        if let id = payload?["userId"] as? Int {
            if let user = UserMapper.getById(id: id) {
                
                let photos = user.getPhotos()
                let JSONArray = photos.map({$0.JSON})
                
                try! response.setBody(json: ["photos" : JSONArray])
            } else {
                try! response.setBody(json: ["error": "Пользователь не существует"])
            }
        } else {
            try! response.setBody(json: ["error": "Неверные данные payload"])
        }
        response.setHeader(.contentType, value: "application/json")
        response.completed()

    }
    
    class func getPhotosWithParameters(request: HTTPRequest, _ response: HTTPResponse) {
        var payload: [String: Any]? = [:]
        do {
            payload = try request.postBodyString?.jsonDecode() as? [String: Any]
        } catch {
            try! response.setBody(json: ["error": "Неверные данные"])
        }
        if let id = payload?["userId"] as? Int, let query = payload?["query"] as? String, let method = payload?["sortMethod"] as? String {
            if let user = UserMapper.getById(id: id) {
                
                let photos = user.getPhotosWithParameters(query: query, method: method)
                let JSONArray = photos.map({$0.JSON})
                
                try! response.setBody(json: ["photos" : JSONArray])
            } else {
                try! response.setBody(json: ["error": "Пользователь не существует"])
            }
        } else {
            try! response.setBody(json: ["error": "Неверные данные payload"])
        }
        response.setHeader(.contentType, value: "application/json")
        response.completed()
        
    }
}
